﻿window.onload = init;


function init() {
    // URL of the TILE SERVER
    const url_carto_cdn = 'http://{1-4}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png';

    // Our map
    const map = new ol.Map({
        target: document.getElementById('id_map'),
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: new ol.View({
            // Centered in Europe
            center: ol.proj.fromLonLat([21, 44]),
            zoom: 6
        })
    });





    // EVENT ON MOUSE CLICK
    map.on('click', function (evt) {

        // Coords of click is evt.coordinate
    //console.log("evt.coordinate: " + evt.coordinate);
        // You must transform the coordinates because evt.coordinate 
        // is by default Web Mercator (EPSG:3857) 
        // and not "usual coords" (EPSG:4326) 
        const coords_click = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
    //console.log("Mouse Click coordinates: " + coords_click);

        // MOUSE CLICK: Longitude
        const lon = coords_click[0];
        // MOUSE CLICK: Latitude
        const lat = coords_click[1];

        // DATA to put in NOMINATIM URL to find address of mouse click location
        const data_for_url = { lon: lon, lat: lat, format: "json", limit: 1 };

        // ENCODED DATA for URL
        const encoded_data = Object.keys(data_for_url).map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(data_for_url[k])
        }).join('&');


        // FULL URL for searching address of mouse click
        const url_nominatim = 'https://nominatim.openstreetmap.org/reverse?' + encoded_data;
    //console.log("URL Request NOMINATIM-Reverse: " + url_nominatim);


        // GET URL REQUEST for ADDRESS
        httpGet(url_nominatim, function (response_text) {

            // JSON Data of the response to the request Nominatim
            const data_json = JSON.parse(response_text);

            // Longitude and latitude
            const res_lon = data_json.lon;
            const res_lat = data_json.lat;

            // All the informations of the address are here
            const res_address = data_json.address;

            // Details depends on the location, country and places
            // For example: in the desert, road or pedestrian is 
            // probably set to undefined because of none...

            const address_city = res_address.city;
            const address_road = res_address.road;
            const address_house_number = res_address.house_number;
            const address_postcode = res_address.postcode;



            console.log("Longitude    : " + res_lon);
            console.log("Longitude    : " + res_lat);
            console.log("City         : " + address_city);
            console.log("Road         : " + address_road);
            console.log("House Number : " + address_house_number);
            console.log("Postcode     : " + address_postcode);


            document.getElementById('ulicaibrojJS').value = address_road + " " + address_house_number;
            document.getElementById('gradJS').value = address_city;
            document.getElementById('postanskibrojJS').value = address_postcode;
            document.getElementById('geoduzinaJS').value = res_lon;
            document.getElementById('geosirinaJS').value = res_lat;

            document.getElementById('mestonalazenjaJS').value = address_road + " " + address_house_number + " " + address_postcode + " " + res_lon + " " + res_lat;
        });
    });




}


function httpGet(url, callback_function) {

    const getRequest = new XMLHttpRequest();
    getRequest.open("get", url, true);

    getRequest.addEventListener("readystatechange", function () {

        // IF RESPONSE is GOOD
        if (getRequest.readyState === 4 && getRequest.status === 200) {

            // Callback for making stuff with the Nominatim response address
            callback_function(getRequest.responseText);
        }
    });

    // Send the request
    getRequest.send();
}

