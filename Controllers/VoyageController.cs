﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Models.Enums;

namespace WebProjekat.Controllers
{
    public class VoyageController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult StaraPonuda()
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Aranzman> selektovaniAranzmani = new List<Aranzman>();

            foreach (Aranzman a in aranzmani)
            {
                var span = DateTime.Now.Subtract(a.DatumPocetka);
                var dani = span.Days;
                if (dani > 0)
                {
                    selektovaniAranzmani.Add(a);
                }
            }

            selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.DatumZavrsetka).ToList();

            return View(selektovaniAranzmani);
        }


        public ActionResult AktivnaPonuda()
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Aranzman> selektovaniAranzmani = new List<Aranzman>();

            foreach(Aranzman a in aranzmani)
            {
                var span = DateTime.Now.Subtract(a.DatumPocetka);
                var dani = span.Days;
                if(dani <= 0)
                {
                    selektovaniAranzmani.Add(a);
                }
            }


            selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.DatumPocetka).ToList();

            return View(selektovaniAranzmani);
        }


        #region Aranzmani

        public ActionResult KreiranjeAranzmana()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KreiranjeAranzmana(Aranzman a, MestoNalazenja m)
        {
            a.CreatorID = Session["UserID"].ToString();
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];


            m.Grad = m.Grad.Replace(' ', '_');
            m.UlicaIBroj = m.UlicaIBroj.Replace(' ', '_');

            a.MestoNalazenja = m;

            foreach (Korisnik k in korisnici)
            {
                if(k.ID == Session["UserID"].ToString())
                {
                    k.ListaKreiranihAranzmana.Add(a);
                }
            }
            
            BazaPodataka.SacuvajAranzman(a);
            aranzmani.Add(a);
            return RedirectToAction("Index", "ManagerPanel");
        }

        public ActionResult InfoAranzmana(string id)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];
            List<Smestaj> slobodniSmestaji = new List<Smestaj>();
            List<Komentar> prenosKomentara = new List<Komentar>();



            foreach (Smestaj s in smestaji)
            {
                slobodniSmestaji.Add(s);            // Zato sto ako menjam direktno listu "smestaji" onda se i menja httpcontext za dalju upotrebu
            }

            foreach (Aranzman a in aranzmani)
            {
                slobodniSmestaji.RemoveAll(x => x.ID == a.Smestaj.ID);
            }

            ViewData["Smestaji"] = slobodniSmestaji;

            foreach (Aranzman a in aranzmani)
            {
                if (a.ID == id)
                {       
                    foreach (Komentar k in komentari)
                    {
                        if (k.Aranzman.ID == id)
                            prenosKomentara.Add(k);
                    }

                    ViewData["Komentari"] = prenosKomentara;
                    return View(a);
                }
            }


            return View();
        }

        public ActionResult BrisanjeAranzmana(string id)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];

            foreach (Aranzman a in aranzmani)
            {
                if (a.ID == id)
                {
                    foreach (Rezervacija r in rezervacije)
                    {
                        if(r.Aranzman == a)
                        {
                            return RedirectToAction("Index", "ManagerPanel");
                        }
                    }
                    a.Deleted = true;
                    BazaPodataka.UpdateAranzmana(aranzmani);
                }
            }

            return RedirectToAction("Index","ManagerPanel");
        }


        #endregion


        #region Smestaji


        public ActionResult KreiranjeSmestajaID(string id)
        {
            ViewBag.ID = id;

            return View("KreiranjeSmestaja");

        }

        public ActionResult KreiranjeSmestaja()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KreiranjeSmestaja(Smestaj sm)
        {
            Random rand = new Random();
            sm.ID = (rand.Next(1000) + 1).ToString();           // Zato sto se u praznom konstruktoru ID stavlja na null PAZI NA OVO



            sm.CreatorID = Session["UserID"].ToString();
            sm.ListaSmestajnihJedinica = new List<SmestajnaJedinica>();
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];

            BazaPodataka.SacuvajSmestaj(sm);
            smestaji.Add(sm);
            return RedirectToAction("Index", "ManagerPanel");

        }

        

        public ActionResult PrikazSmestaja()
        {
            List<Smestaj> smestaji = BazaPodataka.UcitavanjeSmestaja(Server.MapPath("~/App_Data/smestaji.txt"));
            List<Smestaj> prenosSmestaji = new List<Smestaj>();

            foreach (Smestaj sm in smestaji)
            {
                if (sm.Deleted == false)
                {
                    prenosSmestaji.Add(sm);
                }
            }

            return View(prenosSmestaji);
        }


        public ActionResult InfoSmestaja(string id)
        {
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];



            foreach(Smestaj s in smestaji)
            {
                if (s.ID == id)
                {
                    return View(s);
                }

            }
            return View("Index");

        }

        public ActionResult DodeliSmestaj()
        {

            return View("Index");
        }

        [HttpPost]
        public ActionResult DodeliSmestaj(string idSmestaja, string idAranzmana)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];

            foreach (Aranzman a in aranzmani)
            {
                if(a.ID == idAranzmana)
                {
                    foreach(Smestaj s in smestaji)
                    {
                        if(s.ID == idSmestaja)
                        {
                            a.Smestaj = s;

                            BazaPodataka.UpdateAranzmana(aranzmani);
                            return RedirectToAction("InfoAranzmana", "Voyage", new { id = a.ID });
                        }
                    }
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult OdvojSmestaj(string id)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];

            foreach (Aranzman a in aranzmani)
            {
                if(a.ID == id)
                {
                    a.Smestaj = new Smestaj();

                    BazaPodataka.UpdateAranzmana(aranzmani);

                    return RedirectToAction("InfoAranzmana", "Voyage", new { id = a.ID });
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult BrisanjeSmestaja(string id)
        {
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];

            foreach (Smestaj sm in smestaji)
            {
                if (sm.ID == id && sm.Deleted == false)
                {
                    sm.Deleted = true;
                }
            }

            BazaPodataka.UpdateSmestaja(smestaji);
            return View("PrikazSmestaja");
        }

        #endregion


        #region Smestajne jedinice

        public ActionResult KreiranjeSmestajneJediniceSAID(string id)
        {

            ViewBag.ID = $"{id}";
            return View("KreiranjeSmestajneJedinice");
        }

        [HttpPost]
        public ActionResult KreiranjeSmestajneJedinice(SmestajnaJedinica sj, string idSmestaja)
        {
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];

            Random rand = new Random();
            sj.ID = (rand.Next(1000) + 1).ToString();           // Zato sto se u praznom konstruktoru ID stavlja na null PAZI NA OVO

            SmestajnaJedinica jedinica = new SmestajnaJedinica(
                sj.ID,
                sj.IDSmestaja,
                sj.DozvoljenBrojGostiju,
                sj.DozvoljeniLjubimci,
                sj.CenaJedinice,
                false,
                false);

            foreach(Smestaj s in smestaji)
            {
                if(s.ID == idSmestaja)
                {
                    s.ListaSmestajnihJedinica.Add(sj);
                }
            }

            BazaPodataka.UpdateSmestaja(smestaji);
            BazaPodataka.SacuvajSmestajnuJedinicu(jedinica);
            return View("PrikazSmestaja", smestaji);

        }

        [HttpPost]
        public ActionResult ObrisiSmestajnuJedinicu(string idSmestajne)
        {
            List<SmestajnaJedinica> smestajneJedinice = (List<SmestajnaJedinica>)HttpContext.Application["smestajneJedinice"];
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];
            Smestaj prenosSmestaja = new Smestaj();


            foreach (SmestajnaJedinica sj in smestajneJedinice)
            {
                if(sj.ID == idSmestajne && sj.Zauzeta == false)
                {
                    sj.Deleted = true;
                    foreach (Smestaj s in smestaji)
                    {
                        if (s.ID == sj.IDSmestaja)
                            prenosSmestaja = s;


                        return View("InfoSmestaja", prenosSmestaja);
                    }
                    return View("Index");
                }
            }

            return View("Index");
        }


        #endregion


        #region Rezervacije i otkazivanje

        [HttpPost]
        public ActionResult Rezervisi(string idJedinice)
        {
            List<SmestajnaJedinica> smestajneJedinice = (List<SmestajnaJedinica>)HttpContext.Application["smestajneJedinice"];
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];

            Aranzman ar = new Aranzman();

            SmestajnaJedinica prosledjenaJedinica = new SmestajnaJedinica();

            string idSmestaja = "";
            foreach (SmestajnaJedinica sj in smestajneJedinice)
            {
                if (sj.ID == idJedinice)
                {
                    sj.Zauzeta = true;
                    idSmestaja = sj.IDSmestaja;
                    prosledjenaJedinica = sj;
                }
            }

            foreach (Aranzman a in aranzmani)
            {
                if (a.Smestaj.ID == idSmestaja)
                {
                    ar = a;
                }
            }

            Random _rand = new Random();
            string idBroj = "";
            for (int j = 0; j < 15; j++)
            {
                idBroj = String.Concat(idBroj, _rand.Next(10).ToString());
            }

            Rezervacija rezervacija = new Rezervacija(idBroj, (Korisnik)Session["User"], StatusRezervacije.AKTIVNA, ar, prosledjenaJedinica);
            rezervacije.Add(rezervacija);

            foreach (Korisnik k in korisnici)
            {
                if(k.ID == Session["UserID"].ToString())
                {
                    k.ListaRezervacija.Add(rezervacija);
                }
            }

            BazaPodataka.SacuvajRezervaciju(rezervacija);
            BazaPodataka.UpdateKorisnika(korisnici);
            BazaPodataka.UpdateSmestajnihJedinica(smestajneJedinice);

            return RedirectToAction("MojeRezervacije", "Home");
        }

        public ActionResult OtkaziRezervaciju(string idRezervacije)
        {
            List<SmestajnaJedinica> smestajneJedinice = (List<SmestajnaJedinica>)HttpContext.Application["smestajneJedinice"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];

            string idJedinice = "";
            foreach (Rezervacija r in rezervacije)
            {
                if (r.ID == idRezervacije)
                {
                    idJedinice = r.SmestajnaJedinica.ID;
                    r.Status = StatusRezervacije.OTKAZANA;

                    foreach (Korisnik k in korisnici)
                    {
                        if(k.ID == Session["UserID"].ToString())
                        {
                            k.ListaRezervacija.Remove(r);
                        }
                    }

                    break;
                }
            }


            foreach (SmestajnaJedinica sj in smestajneJedinice)
            {
                if (sj.ID == idJedinice)
                {
                    sj.Zauzeta = false;
                }
            }


            BazaPodataka.UpdateSmestajnihJedinica(smestajneJedinice);

            return RedirectToAction("MojeRezervacije", "Home");
        }

        #endregion


        #region Pretrage

        [HttpPost]
        public ActionResult FiltriranjeStarih(DateTime datumPocetkaMin, DateTime datumPocetkaMax, DateTime datumZavrsetkaMin, DateTime datumZavrsetkaMax, TipPrevoza tipPrevoza, TipAranzmana tipAranzmana, string naziv)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Aranzman> filtriraniAranzmani = new List<Aranzman>();

            foreach (Aranzman a in aranzmani)
            {

                if ((DateTime.Compare(a.DatumPocetka, datumPocetkaMin) >= 0) && (DateTime.Compare(a.DatumPocetka, datumPocetkaMax) < 0) && (DateTime.Compare(a.DatumZavrsetka, datumZavrsetkaMin) >= 0) && (DateTime.Compare(a.DatumZavrsetka, datumZavrsetkaMax) < 0) && (a.TipPrevoza == tipPrevoza) && (a.TipAranzmana == tipAranzmana) && (a.Naziv.ToUpper().Contains(naziv.ToUpper())))
                {
                    if (DateTime.Compare(a.DatumPocetka, DateTime.Now) <= 0)
                    {
                        filtriraniAranzmani.Add(a);
                    }
                }
            }


            return View("StaraPonuda", filtriraniAranzmani);
        }


        [HttpPost]
        public ActionResult FiltriranjeAktivnih(DateTime datumPocetkaMin, DateTime datumPocetkaMax, DateTime datumZavrsetkaMin, DateTime datumZavrsetkaMax, TipPrevoza tipPrevoza, TipAranzmana tipAranzmana, string naziv)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Aranzman> filtriraniAranzmani = new List<Aranzman>();

            foreach (Aranzman a in aranzmani)
            {

                if ((DateTime.Compare(a.DatumPocetka, datumPocetkaMin) >= 0) && (DateTime.Compare(a.DatumPocetka, datumPocetkaMax) < 0) && (DateTime.Compare(a.DatumZavrsetka, datumZavrsetkaMin) >= 0) && (DateTime.Compare(a.DatumZavrsetka, datumZavrsetkaMax) < 0) && (a.TipPrevoza == tipPrevoza) && (a.TipAranzmana == tipAranzmana) && ((a.Naziv.ToUpper()).Contains(naziv.ToUpper())))
                {
                    if (DateTime.Compare(a.DatumPocetka, DateTime.Now) > 0)
                    {
                        filtriraniAranzmani.Add(a);
                    }
                }
            }


            return View("AktivnaPonuda", filtriraniAranzmani);
        }

        [HttpPost]
        public ActionResult FiltriranjeSJ(int cenaMin, int cenaMax, int brojGostiju, bool dozvoljeniLjubimci, string idSmestaja)
        {
            List<SmestajnaJedinica> smestajneJedinice = (List<SmestajnaJedinica>)HttpContext.Application["smestajneJedinice"];
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];
            List<SmestajnaJedinica> filtriraneJedinice = new List<SmestajnaJedinica>();

            foreach (SmestajnaJedinica sj in smestajneJedinice)
            {
                if (sj.IDSmestaja == idSmestaja)
                {
                    if (cenaMin <= sj.CenaJedinice && cenaMax >= sj.CenaJedinice && sj.DozvoljenBrojGostiju == brojGostiju && sj.DozvoljeniLjubimci == dozvoljeniLjubimci)
                    {
                        filtriraneJedinice.Add(sj);
                    }
                }
            }

            Smestaj prenosSmestaja = new Smestaj();
            foreach (Smestaj s in smestaji)
            {
                if (s.ID == idSmestaja)
                {
                    prenosSmestaja = s;
                }
            }

            prenosSmestaja.ListaSmestajnihJedinica = filtriraneJedinice;

            return View("InfoSmestaja", prenosSmestaja);
        }

        public ActionResult FiltriranjeSmestaja(string naziv, bool wifi = false, bool bazen = false, bool spa = false, bool invalidi = false, TipSmestaja tipSmestaja = TipSmestaja.HOTEL)
        {
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];
            List<Smestaj> prenosSmestaja = new List<Smestaj>();

            

            foreach (Smestaj s in smestaji)
            {
                if(s.CreatorID == Session["UserID"].ToString())
                {
                    if ((s.Naziv.ToUpper()).Contains(naziv.ToUpper()))
                    {
                        if (wifi == true && s.PostojanjeWiFia != wifi)
                            continue;
                        if (bazen == true && s.PostojanjeBazena != bazen)
                            continue;
                        if (spa == true && s.PostojanjeSpa != spa)
                            continue;
                        if (invalidi == true && s.PrilagodjenoInvalidima != invalidi)
                            continue;

                        prenosSmestaja.Add(s);
                    }
                }
            }


            return View("PrikazSmestaja", prenosSmestaja);
        }


        #endregion


        #region Sortiranja
        public ActionResult SortiranjeAktivnih(string kriterijum)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Aranzman> selektovaniAranzmani = new List<Aranzman>();

            foreach (Aranzman a in aranzmani)
            {
                var span = DateTime.Now.Subtract(a.DatumPocetka);
                var dani = span.Days;
                if (dani <= 0)
                {
                    selektovaniAranzmani.Add(a);
                }
            }


            switch (kriterijum)
            {
                case "nameDesc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderByDescending(x => x.Naziv).ToList();
                    break;
                case "nameAsc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.Naziv).ToList();
                    break;
                case "startDesc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderByDescending(x => x.DatumPocetka).ToList();
                    break;
                case "startAsc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.DatumPocetka).ToList();
                    break;
                case "endDesc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderByDescending(x => x.DatumZavrsetka).ToList();
                    break;
                case "endAsc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.DatumZavrsetka).ToList();
                    break;
                default:
                    break;
            }

            return View("AktivnaPonuda", selektovaniAranzmani);
        }

        public ActionResult SortiranjeStarih(string kriterijum)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Aranzman> selektovaniAranzmani = new List<Aranzman>();

            foreach (Aranzman a in aranzmani)
            {
                var span = DateTime.Now.Subtract(a.DatumPocetka);
                var dani = span.Days;
                if (dani > 0)
                {
                    selektovaniAranzmani.Add(a);
                }
            }


            switch (kriterijum)
            {
                case "nameDesc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderByDescending(x => x.Naziv).ToList();
                    break;
                case "nameAsc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.Naziv).ToList();
                    break;
                case "startDesc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderByDescending(x => x.DatumPocetka).ToList();
                    break;
                case "startAsc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.DatumPocetka).ToList();
                    break;
                case "endDesc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderByDescending(x => x.DatumZavrsetka).ToList();
                    break;
                case "endAsc":
                    selektovaniAranzmani = selektovaniAranzmani.OrderBy(x => x.DatumZavrsetka).ToList();
                    break;
                default:
                    break;
            }

            return View("StaraPonuda", selektovaniAranzmani);
        }

        public ActionResult SortiranjeSJ(string kriterijum, string idSmestaja)
        {
            List<SmestajnaJedinica> smestajneJedinice = (List<SmestajnaJedinica>)HttpContext.Application["smestajneJedinice"];
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];
            List<SmestajnaJedinica> filtriraneJedinice = new List<SmestajnaJedinica>();

            foreach (SmestajnaJedinica sj in smestajneJedinice)
            {
                if (sj.IDSmestaja == idSmestaja)
                {
                    filtriraneJedinice.Add(sj);
                }
            }

            switch (kriterijum)
            {
                case "guestDesc":
                    filtriraneJedinice = filtriraneJedinice.OrderBy(x => x.DozvoljenBrojGostiju).ToList();
                    break;
                case "guestAsc":
                    filtriraneJedinice = filtriraneJedinice.OrderByDescending(x => x.DozvoljenBrojGostiju).ToList();
                    break;
                case "priceDesc":
                    filtriraneJedinice = filtriraneJedinice.OrderBy(x => x.CenaJedinice).ToList();
                    break;
                case "priceAsc":
                    filtriraneJedinice = filtriraneJedinice.OrderByDescending(x => x.CenaJedinice).ToList();
                    break;
                default:
                    break;
            }

            Smestaj tempSmestaj = new Smestaj();
            foreach (Smestaj s in smestaji)
            {
                if (s.ID == idSmestaja)
                {
                    tempSmestaj = s;
                }
            }
            tempSmestaj.ListaSmestajnihJedinica = filtriraneJedinice;

            return View("InfoSmestaja", tempSmestaj);
        }

        public ActionResult SortiranjeSmestaja(string kriterijum)
        {
            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Application["smestaji"];
            List<Smestaj> filtriraniSmestaji = new List<Smestaj>();

            foreach (Smestaj s in smestaji)
            {
                if (s.CreatorID == Session["UserID"].ToString())
                {
                    filtriraniSmestaji.Add(s);
                }
            }

            switch (kriterijum)
            {
                case "nameDesc":
                    filtriraniSmestaji = filtriraniSmestaji.OrderBy(x => x.Naziv).ToList();
                    break;
                case "nameAsc":
                    filtriraniSmestaji = filtriraniSmestaji.OrderByDescending(x => x.Naziv).ToList();
                    break;
                case "totDesc":
                    filtriraniSmestaji = filtriraniSmestaji.OrderBy(x => x.ListaSmestajnihJedinica.Count).ToList();
                    break;
                case "totAsc":
                    filtriraniSmestaji = filtriraniSmestaji.OrderByDescending(x => x.ListaSmestajnihJedinica.Count).ToList();
                    break;
                case "freeDesc":
                    filtriraniSmestaji = filtriraniSmestaji.OrderBy(x => x.ListaSmestajnihJedinica.Count).ToList();
                    break;
                case "freeAsc":
                    filtriraniSmestaji = filtriraniSmestaji.OrderByDescending(x => x.ListaSmestajnihJedinica.Count).ToList();
                    break;
                default:
                    break;
            }


            return View("PrikazSmestaja", filtriraniSmestaji);
        }
        #endregion



        [HttpPost]
        public ActionResult OdobriKomentar(string idKomentara)
        {
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            Aranzman prebacenAranz = new Aranzman();

            foreach (Komentar k in komentari)
            {
                if (k.ID == idKomentara)
                {
                    k.Odobren = true;
                    prebacenAranz = k.Aranzman;
                }
            }

            BazaPodataka.UpdateKomentara(komentari);

            return RedirectToAction("AktivnaPonuda");
        }

    }
}