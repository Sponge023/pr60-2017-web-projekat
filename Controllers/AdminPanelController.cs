﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Models.Enums;

namespace WebProjekat.Controllers
{
    public class AdminPanelController : Controller
    {
        // GET: AdminPanel
        public ActionResult Index()
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];

            return View(korisnici);
        }



        public ActionResult BlokirajKorisnika(string id)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            foreach (Korisnik k in korisnici)
            {
                if(k.ID == id)
                {
                    k.Blokiran = 1;
                }
            }

            BazaPodataka.UpdateKorisnika(korisnici);

            return View("Index", korisnici);
        }


        public ActionResult OdblokirajKorisnika(string id)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            foreach (Korisnik k in korisnici)
            {
                if(k.ID == id)
                {
                    k.Blokiran = 0;
                }
            }

            BazaPodataka.UpdateKorisnika(korisnici);

            return View("Index", korisnici);
        }

        public ActionResult KreiranjeMenadzera()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KreiranjeMenadzera(Korisnik k)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            k.Uloga = Models.Enums.Uloga.MENADZER;
            korisnici.Add(k);
            BazaPodataka.SacuvajKorisnika(k);

            return RedirectToAction("Index","AdminPanel");
        }

        public ActionResult ObrisiKorisnika(string id)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            foreach (Korisnik k in korisnici)
            {
                if (k.ID == id)
                {
                    k.Deleted = true;
                }
            }

            BazaPodataka.UpdateKorisnika(korisnici);

            return View("Index", korisnici);
        }



        [HttpPost]
        public ActionResult FiltriranjeKorisnika(string ime, string prezime, Uloga uloga)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            List<Korisnik> filtriraniKorisnici = new List<Korisnik>();


            foreach (Korisnik k in korisnici)
            {
                if (k.Uloga == uloga && ((k.Ime.ToUpper()).Contains(ime.ToUpper())) && ((k.Prezime.ToUpper()).Contains(prezime.ToUpper())))
                {
                    filtriraniKorisnici.Add(k);
                }
            }


            return View("Index",  filtriraniKorisnici);
        }


        public ActionResult SortiranjeKor(string kriterijum)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];



            switch (kriterijum)
            {
                case "nameDesc":
                    korisnici = korisnici.OrderByDescending(x => x.Ime).ToList();
                    break;
                case "nameAsc":
                    korisnici = korisnici.OrderBy(x => x.Ime).ToList();
                    break;
                case "snameDesc":
                    korisnici = korisnici.OrderByDescending(x => x.Prezime).ToList();
                    break;
                case "snameAsc":
                    korisnici = korisnici.OrderBy(x => x.Prezime).ToList();
                    break;
                case "roleDesc":
                    korisnici = korisnici.OrderByDescending(x => x.Uloga).ToList();
                    break;
                case "roleAsc":
                    korisnici = korisnici.OrderBy(x => x.Uloga).ToList();
                    break;
                default:
                    break;
            }

            return View("Index", korisnici);
        }

    }
}