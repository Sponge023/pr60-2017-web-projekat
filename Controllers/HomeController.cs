﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("AktivnaPonuda","Voyage");
        }


        public ActionResult MojProfil()
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            foreach(Korisnik k in korisnici)
            {
                if(k.ID == Session["UserID"].ToString())
                {
                    return View(k);
                }
            }

            return View();
        }

        public ActionResult IzmeniProfil()
        {
            if (Session["UserID"] != null)
            {
                List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
                foreach (Korisnik k in korisnici)
                {
                    if (k.ID == Session["UserID"].ToString())
                    {
                        return View(k);
                    }
                }
                return View();
            }
            else
                return RedirectToAction("Home", "Index");
            
        }

        [HttpPost]
        public ActionResult IzmeniProfil(Korisnik kor)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            foreach (Korisnik k in korisnici)
            {
                if (k.ID == Session["UserID"].ToString())
                {
                    kor.ID = k.ID;
                    kor.Uloga = k.Uloga;
                    kor.ListaRezervacija = k.ListaRezervacija;
                    kor.ListaKreiranihAranzmana = k.ListaKreiranihAranzmana;
                    korisnici.Remove(k);
                    break;
                }
            }

            korisnici.Add(kor);
            BazaPodataka.UpdateKorisnika(korisnici);

            return RedirectToAction("MojProfil","Home");
        }


        public ActionResult MojeRezervacije()
        {
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Rezervacija> korisnikoveRezervacije = new List<Rezervacija>();

            foreach (Rezervacija rez in rezervacije)
            {
                if(rez.Korisnik == Session["User"])
                {
                    korisnikoveRezervacije.Add(rez);
                }
            }

            return View(korisnikoveRezervacije);
        }

        public ActionResult DodajKomentar()
        {

            return View();
        }
        
        [HttpPost]
        public ActionResult DodajKomentar(string idAranzmana)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];

            foreach (Aranzman a in aranzmani)
            {
                if(a.ID==idAranzmana)
                {
                    ViewData["Aranzman"] = a;
                }
            }


            return View();
        }
        
        [HttpPost]
        public ActionResult DodajKomentarSAID(string sadrzaj, int ocena,string idAranzmana)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["aranzmani"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];


            Korisnik kor = new Korisnik();
            Aranzman ar = new Aranzman();
            foreach (Aranzman a in aranzmani)
            {
                if (a.ID == idAranzmana)
                    ar = a;
            }
            foreach (Korisnik k in korisnici)
            {
                if (k.ID == Session["UserID"].ToString())
                {
                    kor = k;
                }
            }
            
            Random _rand = new Random();
            string idKom = (_rand.Next(1000) + 1).ToString();

            Komentar kom = new Komentar(idKom, kor, ar, sadrzaj, ocena, false);

            BazaPodataka.SacuvajKomentar(kom);
            return RedirectToAction("MojeRezervacije");
        }


    }
}