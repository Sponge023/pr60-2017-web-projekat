﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class AutentifikacijaController : Controller
    {
        public ActionResult Registracija()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Registracija(Korisnik korisnik)
        {
            string izabranPol = Request.Form["biranjePola"].ToString();


            Korisnik kreiraniKorisnik = new Korisnik(korisnik.KorisnickoIme,
                korisnik.Lozinka,
                korisnik.Ime,
                korisnik.Prezime,
                izabranPol,
                korisnik.Email,
                korisnik.DatumRodjenja,
                Models.Enums.Uloga.TURISTA,
                new List<Rezervacija>(),
                new List<Aranzman>(),
                0);

            BazaPodataka.SacuvajKorisnika(kreiraniKorisnik);
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            korisnici.Add(kreiraniKorisnik);

            return RedirectToAction("Prijava", "Autentifikacija");
        }

        public ActionResult Prijava()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Prijava(string korisnickoIme, string lozinka)
        {
            List<Korisnik> pretraga = (List<Korisnik>)HttpContext.Application["korisnici"];
            Korisnik kor = pretraga.Find(k => k.KorisnickoIme.Equals(korisnickoIme) && k.Lozinka.Equals(lozinka));

            if (kor == null)
            {
                ViewBag.Message = $"Neispravni podaci!";
                return View();
            }

            if(kor.Blokiran == 1)
            {
                ViewBag.Message = $"Blokirani ste!";
                return View();
            }


            Session["Username"] = kor.KorisnickoIme;
            Session["Ime"] = kor.Ime;
            Session["UserID"] = kor.ID;
            Session["User"] = kor;
            Session["Role"] = kor.Uloga.ToString();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Odjava()
        {
            Session["Username"] = null;
            Session["Ime"] = "Gost";
            Session["UserID"] = null;
            Session["User"] = null;
            Session["Role"] = null;

            return RedirectToAction("Index", "Home");
        }
    }
}