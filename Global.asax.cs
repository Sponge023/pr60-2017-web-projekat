﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebProjekat.Models;

namespace WebProjekat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            List<SmestajnaJedinica> smestajneJedinice = BazaPodataka.UcitavanjeSmestajnihJedinica(Server.MapPath("~/App_Data/smestajneJedinice.txt"));
            HttpContext.Current.Application["smestajneJedinice"] = smestajneJedinice;

            List<Smestaj> smestaji = BazaPodataka.UcitavanjeSmestaja(Server.MapPath("~/App_Data/smestaji.txt"));
            HttpContext.Current.Application["smestaji"] = smestaji;

            List<Aranzman> aranzmani = BazaPodataka.UcitavanjeAranzmana(Server.MapPath("~/App_Data/aranzmani.txt"));
            HttpContext.Current.Application["aranzmani"] = aranzmani;

            List<Korisnik> korisnici = BazaPodataka.UcitavanjeKorisnika(Server.MapPath("~/App_Data/korisnici.txt"));
            HttpContext.Current.Application["korisnici"] = korisnici;

            List<Rezervacija> rezervacije = BazaPodataka.UcitavanjeRezervacija(Server.MapPath("~/App_Data/rezervacije.txt"));
            HttpContext.Current.Application["rezervacije"] = rezervacije;

            //      Mora zaobilaznim putem i ovako prljavo zato sto kad ucitavas korisnike koristis rezervacije, i obratno

            List<Korisnik> korisnici2 = BazaPodataka.UcitavanjeRezervacijaKorisnika();
            HttpContext.Current.Application["korisnici"] = korisnici2;



            List<Komentar> komentari = BazaPodataka.UcitavanjeKomentara(Server.MapPath("~/App_Data/komentari.txt"));
            HttpContext.Current.Application["komentari"] = komentari;

        }
    }
}
