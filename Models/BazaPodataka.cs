﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using WebProjekat.Models.Enums;

namespace WebProjekat.Models
{
    public class BazaPodataka
    {

        #region Rad sa korisnicima

        public static void SacuvajKorisnika(Korisnik korisnik)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            string rezervacije = "";
            string aranzmani = "";

            foreach (Rezervacija rez in korisnik.ListaRezervacija)
            {
                rezervacije += $"{rez.ID} ";
            }

            foreach (Aranzman ar in korisnik.ListaKreiranihAranzmana)
            {
                aranzmani += $"{ar.ID} ";
            }

            sw.WriteLine($"{korisnik.ID}|{korisnik.KorisnickoIme}|{korisnik.Lozinka}|{korisnik.Ime}|{korisnik.Prezime}|{korisnik.Pol.ToString()}|{korisnik.Email}|{korisnik.DatumRodjenja.ToString("MM'/'dd'/'yyyy")}|{rezervacije}|{aranzmani}|{korisnik.Uloga.ToString()}|{korisnik.Blokiran}|{korisnik.Deleted}");

            sw.Close();
            stream.Close();

            return;
        }

        public static List<Korisnik> UcitavanjeKorisnika(string putanja)
        {
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Current.Application["rezervacije"];
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Current.Application["aranzmani"];

            List<Korisnik> korisnici = new List<Korisnik>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');
                string[] rezIndeks = indeks[8].Split(' ');
                string[] arIndeks = indeks[9].Split(' ');

                List<Rezervacija> listaRezervacija = new List<Rezervacija>();
                
                
                //for (int i = 0; i < rezIndeks.Length; i++)
                //{
                //    foreach (Rezervacija rez in rezervacije)
                //    {
                //        if (rez.ID == rezIndeks[i])                                       // Ne moze da se koriste rezervacije zato sto jos uvek ne postoje, zato
                //            listaRezervacija.Add(rez);                                    // je odradjeno dvostruko ucitavanje u globalu
                //    }
                //}

                List<Aranzman> listaAranzmana = new List<Aranzman>();
                for (int i = 0; i < arIndeks.Length; i++)
                {
                    foreach (Aranzman ar in aranzmani)
                    {
                        if (ar.ID == arIndeks[i])
                            listaAranzmana.Add(ar);
                    }
                }

                Korisnik k = new Korisnik(indeks[0],indeks[1], indeks[2], indeks[3], indeks[4],
                    indeks[5], indeks[6], Convert.ToDateTime(indeks[7]), ((Uloga)Enum.Parse(typeof(Uloga), indeks[10])),listaRezervacija,listaAranzmana, Convert.ToInt32(indeks[11]),Convert.ToBoolean(indeks[12]));
                korisnici.Add(k);
            }
            sr.Close();
            stream.Close();

            return korisnici;
        }

        public static List<Korisnik> UcitavanjeRezervacijaKorisnika()
        {
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Current.Application["rezervacije"];
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Current.Application["aranzmani"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Current.Application["korisnici"];

            foreach (Rezervacija r in rezervacije)
            {
                foreach (Korisnik k in korisnici)
                {
                    if (r.Korisnik.ID == k.ID)
                        k.ListaRezervacija.Add(r);
                }
            }

            return korisnici;
        }

        public static void UpdateKorisnika(List<Korisnik> korisnici)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(filePath, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach(Korisnik korisnik in korisnici)
            {
                string rezervacije = "";
                string aranzmani = "";

                foreach (Rezervacija rez in korisnik.ListaRezervacija)
                {
                    rezervacije += $"{rez.ID} ";
                }

                foreach (Aranzman ar in korisnik.ListaKreiranihAranzmana)
                {
                    aranzmani += $"{ar.ID} ";
                }

                sw.WriteLine($"{korisnik.ID}|{korisnik.KorisnickoIme}|{korisnik.Lozinka}|{korisnik.Ime}|{korisnik.Prezime}|{korisnik.Pol.ToString()}|{korisnik.Email}|{korisnik.DatumRodjenja.ToShortDateString()}|{rezervacije}|{aranzmani}|{korisnik.Uloga.ToString()}|{korisnik.Blokiran}|{korisnik.Deleted}");
            }

            sw.Close();
            stream.Close();

            return;
        }


        #endregion


        #region Rad sa smestajnim jedinicama

        public static void SacuvajSmestajnuJedinicu(SmestajnaJedinica sj)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/smestajneJedinice.txt");
            FileStream stream = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            sw.WriteLine($"{sj.ID}|" +
                $"{sj.DozvoljenBrojGostiju}|" +
                $"{sj.DozvoljeniLjubimci.ToString()}|" +
                $"{sj.CenaJedinice}|" +
                $"{sj.IDSmestaja}|" +
                $"{sj.Deleted.ToString()}|" +
                $"{sj.Zauzeta.ToString()}");

            sw.Close();
            stream.Close();

            return;
        }

        public static List<SmestajnaJedinica> UcitavanjeSmestajnihJedinica(string putanja)
        {
            List<SmestajnaJedinica> smestajneJedinice = new List<SmestajnaJedinica>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');

                if(!(Convert.ToBoolean(indeks[5])))
                {
                    SmestajnaJedinica s = new SmestajnaJedinica(indeks[0], indeks[4], Convert.ToInt32(indeks[1]), Convert.ToBoolean(indeks[2]), Convert.ToDouble(indeks[3]),Convert.ToBoolean(indeks[5]), Convert.ToBoolean(indeks[6]));
                    smestajneJedinice.Add(s);
                }
            }
            sr.Close();
            stream.Close();


            return smestajneJedinice;
        }

        public static void UpdateSmestajnihJedinica(List<SmestajnaJedinica> smestajneJedinice)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/smestajneJedinice.txt");
            FileStream stream = new FileStream(filePath, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach (SmestajnaJedinica sj in smestajneJedinice)
            {
                sw.WriteLine($"{sj.ID}|" +
                $"{sj.DozvoljenBrojGostiju}|" +
                $"{sj.DozvoljeniLjubimci.ToString()}|" +
                $"{sj.CenaJedinice}|" +
                $"{sj.IDSmestaja}|" +
                $"{sj.Deleted.ToString()}|" +
                $"{sj.Zauzeta.ToString()}");

            }


            sw.Close();
            stream.Close();

            return;
        }

        #endregion


        #region Rad sa smestajima
        public static void SacuvajSmestaj(Smestaj smestaj)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/smestaji.txt");
            FileStream stream = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            string ideviSmestajnih = "";
            foreach(SmestajnaJedinica sj in smestaj.ListaSmestajnihJedinica)
            {
                ideviSmestajnih += $"{sj.ID} ";
            }

            sw.WriteLine($"{smestaj.ID}|" +
                $"{smestaj.TipSmestaja.ToString()}|" +
                $"{smestaj.Naziv}|" +
                $"{smestaj.BrojZvezdica.ToString()}|" +
                $"{smestaj.PostojanjeBazena.ToString()}|" +
                $"{smestaj.PostojanjeSpa.ToString()}|" +
                $"{smestaj.PrilagodjenoInvalidima.ToString()}|" +
                $"{smestaj.PostojanjeWiFia.ToString()}|" +
                $"{ideviSmestajnih}|" +
                $"{smestaj.CreatorID}|" +
                $"{smestaj.Deleted.ToString()}");

            sw.Close();
            stream.Close();

            return;
        }

        public static List<Smestaj> UcitavanjeSmestaja(string putanja)
        {
            List<SmestajnaJedinica> smestajne = (List<SmestajnaJedinica>)HttpContext.Current.Application["smestajneJedinice"];


            List<Smestaj> smestaji = new List<Smestaj>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');
                string[] indeksiPart = indeks[8].Split(' ');
                List<SmestajnaJedinica> listaSmestajnih = new List<SmestajnaJedinica>();

                for (int i = 0; i < indeksiPart.Length; i++)
                {
                    foreach (SmestajnaJedinica sj in smestajne)
                    {
                        if(sj.ID == indeksiPart[i])
                        {
                            listaSmestajnih.Add(sj);
                        }
                    }
                }


                Smestaj s = new Smestaj(indeks[0], (TipSmestaja)Enum.Parse(typeof(TipSmestaja), indeks[1]), indeks[2], Convert.ToInt32(indeks[3]), Convert.ToBoolean(indeks[4]), Convert.ToBoolean(indeks[5]), Convert.ToBoolean(indeks[6]), Convert.ToBoolean(indeks[7]), listaSmestajnih, indeks[9],Convert.ToBoolean(indeks[10]));
                smestaji.Add(s);
            }
            sr.Close();
            stream.Close();

            //646|HOTEL|asd|5|True|False|True|False| |476

            return smestaji;
        }

        internal static void UpdateSmestaja(List<Smestaj> smestaji)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/smestaji.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            

            foreach (Smestaj smestaj in smestaji)
            {
                string ideviSmestajnih = "";
                foreach (SmestajnaJedinica sj in smestaj.ListaSmestajnihJedinica)
                {
                    ideviSmestajnih += $"{sj.ID} ";
                }

                sw.WriteLine($"{smestaj.ID}|" +
                $"{smestaj.TipSmestaja.ToString()}|" +
                $"{smestaj.Naziv}|" +
                $"{smestaj.BrojZvezdica.ToString()}|" +
                $"{smestaj.PostojanjeBazena.ToString()}|" +
                $"{smestaj.PostojanjeSpa.ToString()}|" +
                $"{smestaj.PrilagodjenoInvalidima.ToString()}|" +
                $"{smestaj.PostojanjeWiFia.ToString()}|" +
                $"{ideviSmestajnih}|" +
                $"{smestaj.CreatorID}|" +
                $"{smestaj.Deleted.ToString()}");

            }

            sw.Close();
            stream.Close();
        }


        #endregion


        #region Rad sa aranzmanima

        public static void SacuvajAranzman(Aranzman ar)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/aranzmani.txt");
            FileStream stream = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            string tempSmestajID = "";
            if(ar.Smestaj.ID == null)
            {
                tempSmestajID = "null";
            }
            else
            {
                tempSmestajID = ar.Smestaj.ID;
            }

            sw.WriteLine($"{ar.ID}|" +
                $"{ar.Naziv}|" +
                $"{ar.TipAranzmana.ToString()}|" +
                $"{ar.TipPrevoza.ToString()}|" +
                $"{ar.Lokacija}|" +
                $"{ar.DatumPocetka.ToString("MM'/'dd'/'yyyy")}|" +
                $"{ar.DatumZavrsetka.ToString("MM'/'dd'/'yyyy")}|" +
                $"{ar.MestoNalazenja.UlicaIBroj} {ar.MestoNalazenja.Grad} {ar.MestoNalazenja.PostanskiBroj} {ar.MestoNalazenja.GeoDuzina} {ar.MestoNalazenja.GeoSirina}|" +
                $"{ar.VremeNalazenja}|" +
                $"{ar.MaksimalnoPutnika}|" +
                $"{ar.OpisAranzmana}|" +
                $"{ar.ProgramPutovanja}|" +
                $"{ar.FileName}|" +
                $"{tempSmestajID}|" +
                $"{ar.CreatorID}|" +
                $"{ar.Deleted.ToString()}");

            sw.Close();
            stream.Close();

            return;
        }

        public static List<Aranzman> UcitavanjeAranzmana(string putanja)
        {
            List<Aranzman> aranzmani = new List<Aranzman>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";


            List<Smestaj> smestaji = (List<Smestaj>)HttpContext.Current.Application["smestaji"];

            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');
                string[] indeksiMesta = indeks[7].Split(' ');

                Smestaj sm = new Smestaj();
                foreach(Smestaj s in smestaji)
                {
                    if(s.ID == indeks[13])
                    {
                        sm = s;
                    }
                }


                Aranzman a = new Aranzman(indeks[0],
                    indeks[1],
                    (TipAranzmana)Enum.Parse(typeof(TipAranzmana), indeks[2]),
                    (TipPrevoza)Enum.Parse(typeof(TipPrevoza), indeks[3]),
                    indeks[4],
                    Convert.ToDateTime(indeks[5]),
                    Convert.ToDateTime(indeks[6]),
                    new MestoNalazenja(indeksiMesta[0], indeksiMesta[1], Convert.ToInt32(indeksiMesta[2]), Convert.ToDouble(indeksiMesta[3]), Convert.ToDouble(indeksiMesta[4])),
                    Convert.ToDateTime(indeks[8]),
                    Convert.ToInt32(indeks[9]),
                    indeks[10],
                    indeks[11],
                    indeks[12],
                    sm,
                    indeks[14],
                    Convert.ToBoolean(indeks[15]));

                aranzmani.Add(a);
            }
            sr.Close();
            stream.Close();


            return aranzmani;
        }

        public static void UpdateAranzmana(List<Aranzman> aranzmani)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/aranzmani.txt");
            FileStream stream = new FileStream(filePath, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);


            foreach (Aranzman ar in aranzmani)
            {

                string tempSmestajID = "";
                if (ar.Smestaj.ID == null)
                {
                    tempSmestajID = "null";
                }
                else
                {
                    tempSmestajID = ar.Smestaj.ID;
                }

                sw.WriteLine($"{ar.ID}|" +
                    $"{ar.Naziv}|" +
                    $"{ar.TipAranzmana.ToString()}|" +
                    $"{ar.TipPrevoza.ToString()}|" +
                    $"{ar.Lokacija}|" +
                    $"{ar.DatumPocetka.ToShortDateString()}|" +
                    $"{ar.DatumZavrsetka.ToShortDateString()}|" +
                    $"{ar.MestoNalazenja.UlicaIBroj} {ar.MestoNalazenja.Grad} {ar.MestoNalazenja.PostanskiBroj} {ar.MestoNalazenja.GeoDuzina} {ar.MestoNalazenja.GeoSirina}|" +
                    $"{ar.VremeNalazenja}|" +
                    $"{ar.MaksimalnoPutnika}|" +
                    $"{ar.OpisAranzmana}|" +
                    $"{ar.ProgramPutovanja}|" +
                    $"{ar.FileName}|" +
                    $"{tempSmestajID}|" +
                    $"{ar.CreatorID}|" +
                    $"{ar.Deleted.ToString()}");
            }

            sw.Close();
            stream.Close();

            return;
        }

        #endregion


        #region Rad sa rezervacijama

        public static void SacuvajRezervaciju(Rezervacija rez)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/rezervacije.txt");
            FileStream stream = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            sw.WriteLine($"{rez.ID}|{rez.Korisnik.ID}|{rez.Status.ToString()}|{rez.Aranzman.ID}|{rez.SmestajnaJedinica.ID}");

            sw.Close();
            stream.Close();

            return;
        }

        public static List<Rezervacija> UcitavanjeRezervacija(string putanja)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Current.Application["korisnici"];
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Current.Application["aranzmani"];
            List<SmestajnaJedinica> smestajneJedinice = (List<SmestajnaJedinica>)HttpContext.Current.Application["smestajneJedinice"];


            List<Rezervacija> rezervacije = new List<Rezervacija>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                Aranzman ar = new Aranzman();
                Korisnik kor = new Korisnik();
                SmestajnaJedinica sjed = new SmestajnaJedinica();
                string[] indeks = red.Split('|');


                foreach(Korisnik k in korisnici)
                {
                    if(k.ID == indeks[1])
                    {
                        kor = k;
                        break;
                    }
                }
                foreach (Aranzman a in aranzmani)
                {
                    if (a.ID == indeks[3])
                    {
                        ar = a;
                        break;
                    }
                }
                foreach(SmestajnaJedinica sj in smestajneJedinice)
                {
                    if (sj.ID == indeks[4])
                    {
                        sjed = sj;
                        break;
                    }
                }

                Rezervacija r = new Rezervacija(indeks[0],
                    kor,
                    (StatusRezervacije)Enum.Parse(typeof(StatusRezervacije), indeks[2]),
                    ar,
                    sjed);

                rezervacije.Add(r);
            }
            sr.Close();
            stream.Close();

            return rezervacije;
        }


        #endregion


        #region Rad sa komentarima

        public static void SacuvajKomentar(Komentar k)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/komentari.txt");
            FileStream stream = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            sw.WriteLine($"{k.ID}|" +
                $"{k.Aranzman.ID}|" +
                $"{k.Korisnik.ID}|" +
                $"{k.Sadrzaj}|" +
                $"{k.Ocena}|" +
                $"{k.Odobren.ToString()}");

            sw.Close();
            stream.Close();

            return;
        }


        public static List<Komentar> UcitavanjeKomentara(string putanja)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Current.Application["aranzmani"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Current.Application["korisnici"];

            List<Komentar> komentari = new List<Komentar>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');

                Korisnik kor = new Korisnik();
                Aranzman ar = new Aranzman();

                foreach (Aranzman a in aranzmani)
                {
                    if (indeks[1] == a.ID)
                    {
                        ar = a;
                        break;
                    }
                }
                foreach (Korisnik k in korisnici)
                {
                    if (indeks[2] == k.ID)
                    {
                        kor = k;
                        break;
                    }
                }

                Komentar kom = new Komentar(indeks[0],kor, ar, indeks[3], Convert.ToInt32(indeks[4]),Convert.ToBoolean(indeks[5]));
                komentari.Add(kom);

            }
            sr.Close();
            stream.Close();


            return komentari;
        }

        public static void UpdateKomentara(List<Komentar> komentari)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/komentari.txt");
            FileStream stream = new FileStream(filePath, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach (Komentar k in komentari)
            {
                sw.WriteLine($"{k.ID}|" +
                    $"{k.Aranzman.ID}|" +
                    $"{k.Korisnik.ID}|" +
                    $"{k.Sadrzaj}|" +
                    $"{k.Ocena}|" +
                    $"{k.Odobren}");
            }

            sw.Close();
            stream.Close();

            return;
        }


        #endregion

    }
}