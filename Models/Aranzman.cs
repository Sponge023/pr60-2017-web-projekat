﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models.Enums;

namespace WebProjekat.Models
{
    public class Aranzman
    {
        private readonly Random rand = new Random();

        public Aranzman()
        {
            ID = (rand.Next(1000) + 1).ToString();
            Naziv = "";
            TipAranzmana = TipAranzmana.NAJAM_APARTMANA;
            TipPrevoza = TipPrevoza.OSTALO;
            Lokacija = "";
            DatumPocetka = DateTime.Now;
            DatumZavrsetka = DateTime.Now;
            MestoNalazenja = new MestoNalazenja();
            VremeNalazenja = DateTime.Now;
            MaksimalnoPutnika = 0;
            OpisAranzmana = "";
            ProgramPutovanja = "";
            FileName = "";
            Smestaj = new Smestaj();
            Deleted = false;
        }

        public Aranzman(string naziv, TipAranzmana tipAranzmana, TipPrevoza tipPrevoza, string lokacija, DateTime datumPocetka, DateTime datumZavrsetka, MestoNalazenja mestoNalazenja, DateTime vremeNalazenja, int maksimalnoPutnika, string opisAranzmana, string programPutovanja, string fileName, Smestaj smestaj, string creatorID,bool deleted)
        {
            ID = (rand.Next(1000) + 1).ToString();
            Naziv = naziv;
            TipAranzmana = tipAranzmana;
            TipPrevoza = tipPrevoza;
            Lokacija = lokacija;
            DatumPocetka = datumPocetka;
            DatumZavrsetka = datumZavrsetka;
            MestoNalazenja = mestoNalazenja;
            VremeNalazenja = vremeNalazenja;
            MaksimalnoPutnika = maksimalnoPutnika;
            OpisAranzmana = opisAranzmana;
            ProgramPutovanja = programPutovanja;
            FileName = fileName;
            Smestaj = smestaj;
            CreatorID = creatorID;
            Deleted = deleted;
        }

        public Aranzman(string id, string naziv, TipAranzmana tipAranzmana, TipPrevoza tipPrevoza, string lokacija, DateTime datumPocetka, DateTime datumZavrsetka, MestoNalazenja mestoNalazenja, DateTime vremeNalazenja, int maksimalnoPutnika, string opisAranzmana, string programPutovanja, string fileName, Smestaj smestaj, string creatorID,bool deleted)
        {
            ID = id;
            Naziv = naziv;
            TipAranzmana = tipAranzmana;
            TipPrevoza = tipPrevoza;
            Lokacija = lokacija;
            DatumPocetka = datumPocetka;
            DatumZavrsetka = datumZavrsetka;
            MestoNalazenja = mestoNalazenja;
            VremeNalazenja = vremeNalazenja;
            MaksimalnoPutnika = maksimalnoPutnika;
            OpisAranzmana = opisAranzmana;
            ProgramPutovanja = programPutovanja;
            FileName = fileName;
            Smestaj = smestaj;
            CreatorID = creatorID;
            Deleted = deleted;
        }

        public string ID { get; set; }

        public string CreatorID { get; set; }

        public string Naziv { get; set; }

        public TipAranzmana TipAranzmana { get; set; }

        public TipPrevoza TipPrevoza { get; set; }

        public string Lokacija { get; set; }

        public DateTime DatumPocetka { get; set; }

        public DateTime DatumZavrsetka { get; set; }

        public MestoNalazenja MestoNalazenja { get; set; }

        public DateTime VremeNalazenja { get; set; }

        public int MaksimalnoPutnika { get; set; }

        public string OpisAranzmana { get; set; }

        public string ProgramPutovanja { get; set; }

        public string FileName { get; set; }

        public Smestaj Smestaj { get; set; }

        public bool Deleted { get; set; }
    }
}