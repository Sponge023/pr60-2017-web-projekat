﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Komentar
    {
        public Komentar()
        {
            ID = null;
            Korisnik = new Korisnik();
            Aranzman = new Aranzman();
            Sadrzaj = "";
            Ocena = 0;
            Odobren = false;
        }

        public Komentar(string id,Korisnik korisnik, Aranzman aranzman, string sadrzaj, int ocena, bool odobren)
        {
            Korisnik = korisnik;
            Aranzman = aranzman;
            Sadrzaj = sadrzaj;
            Ocena = ocena;
            Odobren = odobren;
            ID = id;
        }

        public Korisnik Korisnik { get; set; }

        public Aranzman Aranzman { get; set; }

        public string Sadrzaj { get; set; }

        public int Ocena { get; set; }

        public bool Odobren { get; set; }
        
        public string ID { get; set; }
    }
}