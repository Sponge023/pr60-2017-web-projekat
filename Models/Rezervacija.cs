﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models.Enums;

namespace WebProjekat.Models
{
    public class Rezervacija
    {
        Random _rand = new Random();

    public Rezervacija()
        {
            string idBroj = "";
            for (int j = 0; j < 15; j++)
            {
                idBroj = String.Concat(idBroj, _rand.Next(10).ToString());
            }
            ID = idBroj;
            Korisnik = new Korisnik();
            Status = StatusRezervacije.OTKAZANA;
            Aranzman = new Aranzman();
            SmestajnaJedinica = new SmestajnaJedinica();
        }

        public Rezervacija(string id, Korisnik korisnik, StatusRezervacije status, Aranzman aranzman, SmestajnaJedinica smestajnaJedinica)
        {
            ID = id;
            Korisnik = korisnik;
            Status = status;
            Aranzman = aranzman;
            SmestajnaJedinica = smestajnaJedinica;
        }

        public string ID { get; set; }

        public Korisnik Korisnik { get; set; }

        public StatusRezervacije Status { get; set; }

        public Aranzman Aranzman { get; set; }

        public SmestajnaJedinica SmestajnaJedinica { get; set; }


    }
}