﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class SmestajnaJedinica
    {
        public SmestajnaJedinica()
        {
            IDSmestaja = "";
            DozvoljenBrojGostiju = 0;
            DozvoljeniLjubimci = false;
            CenaJedinice = 0;
            Deleted = false;
            ID = null;
            Zauzeta = false;
        }

        public SmestajnaJedinica(string id,string idSmestaja, int dozvoljenBrojGostiju, bool dozvoljeniLjubimci, double cenaJedinice, bool deleted, bool zauzeta)
        {
            IDSmestaja = idSmestaja;
            DozvoljenBrojGostiju = dozvoljenBrojGostiju;
            DozvoljeniLjubimci = dozvoljeniLjubimci;
            CenaJedinice = cenaJedinice;
            Deleted = deleted;
            ID = id;
            Zauzeta = zauzeta;
        }

        public string IDSmestaja { get; set; }

        public int DozvoljenBrojGostiju { get; set; }

        public bool DozvoljeniLjubimci { get; set; }

        public double CenaJedinice { get; set; }

        public bool Deleted { get; set; }
    
        public string ID { get; set; }

        public bool Zauzeta { get; set; }
    }
}