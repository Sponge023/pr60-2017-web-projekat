﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class MestoNalazenja
    {
        public MestoNalazenja()
        {
            UlicaIBroj = "UB";
            Grad = "GR";
            PostanskiBroj = 0;
            GeoDuzina = 0;
            GeoSirina = 0;
        }

        public MestoNalazenja(string ulicaIBroj, string grad, int postanskiBroj, double geoDuzina, double geoSirina)
        {
            UlicaIBroj = ulicaIBroj;
            Grad = grad;
            PostanskiBroj = postanskiBroj;
            GeoDuzina = geoDuzina;
            GeoSirina = geoSirina;
        }

        public string UlicaIBroj { get; set; }

        public string Grad { get; set; }

        public int PostanskiBroj { get; set; }

        public double GeoDuzina { get; set; }

        public double GeoSirina { get; set; }


    }
}