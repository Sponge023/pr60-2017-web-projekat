﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models.Enums;

namespace WebProjekat.Models
{
    public class Smestaj
    {
        private readonly Random rand = new Random();

        public Smestaj()
        {
            ID = null;
            TipSmestaja = TipSmestaja.VILA;
            Naziv = "";
            BrojZvezdica = 0;
            PostojanjeBazena = false;
            PostojanjeSpa = false;
            PrilagodjenoInvalidima = false;
            PostojanjeWiFia = false;
            ListaSmestajnihJedinica = new List<SmestajnaJedinica>();
            CreatorID = "0";
            Deleted = false;
        }
        public Smestaj(TipSmestaja tipSmestaja, string naziv, int brojZvezdica, bool postojanjeBazena, bool postojanjeSpa, bool prilagodjenoInvalidima, bool postojanjeWiFia, List<SmestajnaJedinica> listaSmestajnihJedinica, string creatorID, bool deleted)
        {
            ID = (rand.Next(1000) + 1).ToString();
            TipSmestaja = tipSmestaja;
            Naziv = naziv;
            BrojZvezdica = brojZvezdica;
            PostojanjeBazena = postojanjeBazena;
            PostojanjeSpa = postojanjeSpa;
            PrilagodjenoInvalidima = prilagodjenoInvalidima;
            PostojanjeWiFia = postojanjeWiFia;
            ListaSmestajnihJedinica = listaSmestajnihJedinica;
            CreatorID = creatorID;
            Deleted = deleted;
        }

        public Smestaj(string id,TipSmestaja tipSmestaja, string naziv, int brojZvezdica, bool postojanjeBazena, bool postojanjeSpa, bool prilagodjenoInvalidima, bool postojanjeWiFia, List<SmestajnaJedinica> listaSmestajnihJedinica, string creatorID, bool deleted)
        {
            ID = id;
            TipSmestaja = tipSmestaja;
            Naziv = naziv;
            BrojZvezdica = brojZvezdica;
            PostojanjeBazena = postojanjeBazena;
            PostojanjeSpa = postojanjeSpa;
            PrilagodjenoInvalidima = prilagodjenoInvalidima;
            PostojanjeWiFia = postojanjeWiFia;
            ListaSmestajnihJedinica = listaSmestajnihJedinica;
            CreatorID = creatorID;
            Deleted = deleted;
        }

        public string CreatorID { get; set; }

        public string ID { get; set; }

        public TipSmestaja TipSmestaja { get; set; }

        public string Naziv { get; set; }

        public int BrojZvezdica { get; set; }

        public bool PostojanjeBazena { get; set; }

        public bool PostojanjeSpa { get; set; }

        public bool PrilagodjenoInvalidima { get; set; }

        public bool PostojanjeWiFia { get; set; }

        public List<SmestajnaJedinica> ListaSmestajnihJedinica { get; set; }

        public bool Deleted { get; set; }
    }
}