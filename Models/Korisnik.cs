﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models.Enums;

namespace WebProjekat.Models
{
    public class Korisnik
    {
        private readonly Random rand = new Random();

        public Korisnik()
        {
            ID = (rand.Next(1000) + 1).ToString();
            KorisnickoIme = "";
            Lozinka = "";
            Ime = "";
            Prezime = "";
            Pol = "";
            Email = "";
            DatumRodjenja = DateTime.Now;
            Uloga = Uloga.GOST;
            ListaRezervacija = new List<Rezervacija>();
            ListaKreiranihAranzmana = new List<Aranzman>();
            Blokiran = 0;
            Deleted = false;
        }

        public Korisnik(string korisnickoIme, string lozinka, string ime, string prezime, string pol, string email, DateTime datumRodjenja, Uloga uloga, List<Rezervacija> listaRezervacija, List<Aranzman> listaKreiranihAranzmana, int blokiran)
        {
            ID = (rand.Next(1000) + 1).ToString();
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Email = email;
            DatumRodjenja = datumRodjenja;
            Uloga = uloga;
            ListaRezervacija = listaRezervacija;
            ListaKreiranihAranzmana = listaKreiranihAranzmana;
            Blokiran = blokiran;
            Deleted = false;
        }

        public Korisnik(string id, string korisnickoIme, string lozinka, string ime, string prezime, string pol, string email, DateTime datumRodjenja, Uloga uloga, List<Rezervacija> listaRezervacija, List<Aranzman> listaKreiranihAranzmana, int blokiran, bool deleted)
        {
            ID = id;
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Email = email;
            DatumRodjenja = datumRodjenja;
            Uloga = uloga;
            ListaRezervacija = listaRezervacija;
            ListaKreiranihAranzmana = listaKreiranihAranzmana;
            Blokiran = blokiran;
            Deleted = deleted;
        }

        public string ID { get; set; }


        public string KorisnickoIme { get; set; }

        public string Lozinka { get; set; }

        public string Ime { get; set; }

        public string Prezime { get; set; }

        public string Pol { get; set; }

        public string Email { get; set; }

        public DateTime DatumRodjenja { get; set; }

        public Uloga Uloga { get; set; }

        public List<Rezervacija> ListaRezervacija { get; set; }

        public List<Aranzman> ListaKreiranihAranzmana { get; set; }

        public int Blokiran { get; set; }

        public bool Deleted { get; set; }

    }
}