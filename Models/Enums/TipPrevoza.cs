﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models.Enums
{
    public enum TipPrevoza
    {
        AUTOBUS,
        AVION,
        AUTOBUS_AVION,
        INDIVIDUALAN,
        OSTALO
    }
}