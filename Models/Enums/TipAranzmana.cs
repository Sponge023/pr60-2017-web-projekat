﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models.Enums
{
    public enum TipAranzmana
    {
        NOCENJE_SA_DORUCKOM,
        POLUPANSION,
        PUN_PANSION,
        ALL_INCLUSIVE,
        NAJAM_APARTMANA
    }
}